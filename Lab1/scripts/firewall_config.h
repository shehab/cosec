#####################################################
#						    #
# This is the script concerned with configuring the #
# iptables. Here is a set of goals we aim to 	    #
# achieve:					    #
#						    #
# 1.	The default will be to drop all connections #
# 2.	We will allow connections to ssh (port 22)  #
# 		while logging them, with the prefix #
#               "ssh connection attempt"	    #
# 3.	Then we will allow ICMP messages, but log   #
#		them, since that is something we    # 
#		are asked			    #
#		for.				    #
# 4.	Next we will allow traffic going in and out #
#		through the http port (port 80).    #
# 5.	To allow people on the system to still use  #
# 		the internet, we will allow outgoing#
#		traffic through the https port 	    #
#		(port 443)	 		    #
#		as well as incoming traffic through #
#		previously established connections. #
# 6.	Next, to detect DoS attack, we will allow   #
#		the same ip to connect to the server#
#		only up to 50 times per minute.	    #
#		This will then also be logged, and  #
#		the web admin will be notified.     #
#						    #
# ------------------------------------------------- #
# By:						    #
# Shehab Zaineldine (NIA 100287939)		    #
# Alvaro Lopez...				    #
#						    #
#####################################################

# First of all we flush the iptables from old rules
sudo iptables -F

#Default Rules
 sudo iptables -P INPUT DROP
 sudo iptables -P FORWARD DROP
 sudo iptables -P OUTPUT DROP

# Now we allow loopback, so the 
 sudo iptables -A INPUT -i lo -j ACCEPT
 sudo iptables -A OUTPUT -o lo -j ACCEPT

#imcp
 sudo iptables -A INPUT -p icmp -m limit --limit 30/second --limit-burst 200 -j LOG --log-prefix " INCOMING PING REQUEST "
  sudo iptables -A INPUT -p icmp -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A INPUT -p icmp -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM ICMP "

  sudo iptables -A OUTPUT -p icmp -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A OUTPUT -p icmp -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK FROM ICMP"

#port 80
  sudo iptables -A INPUT -p tcp --dport 80 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A INPUT -p tcp --dport 80 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM HTTP "	

  sudo iptables -A OUTPUT -p tcp --sport 80 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A OUTPUT -p tcp --sport 80 -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK THROUGH HTTP "	

#port 443
  sudo iptables -A INPUT -p tcp --dport 443 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A INPUT -p tcp --dport 443 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM HTTPS "

  sudo iptables -A OUTPUT -p tcp --sport 443 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A OUTPUT -p tcp --sport 443 -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK THROUGH HTTPS "

#port 53
  sudo iptables -A OUTPUT -p udp --dport 53 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A OUTPUT -p udp --dport 53 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM DNS "

  sudo iptables -A INPUT -p udp --sport 53 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A INPUT -p udp --sport 53 -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK THROUGH DNS "

  sudo iptables -A OUTPUT -p tcp --dport 53 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A OUTPUT -p tcp --dport 53 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM DNS "

  sudo iptables -A INPUT -p tcp --sport 53 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A INPUT -p tcp --sport 53 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK THROUGH DNS "

#port 22
 sudo iptables -A INPUT -p tcp -m tcp --dport 22 -m limit --limit 30/second -j LOG --log-prefix " INCOMING SSH "
  sudo iptables -A INPUT -p tcp -m tcp --dport 22 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A INPUT -p tcp -m tcp --dport 22 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM SSH "

  sudo iptables -A OUTPUT -p tcp -m tcp --sport 22 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A OUTPUT -p tcp -m tcp --sport 22 -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK THROUGH SSH "

sudo service iptables save
