# First thing we have to do is to update the passwd file.
# We add the user admin, who will be the webadmin,
# responsible for starting up and shutting down the server, and
# managing the mysql database.
sudo cp -f passwd /etc/passwd

# Next we need to update the sudoers file.
# Here we have restricted the user web admin from
# having sudo rights outside the tomcat folder.
sudo cp -f sudoers.tmp /etc/sudoers.tmp

# Changing the owner of tomcat and mysql, to be the admin, and no longer cosec
sudo chown -R admin /var/lib/mysql
sudo chown -R admin /usr/local/tomcat

# Changing the permissions of the folders mysql and tomcat recursively.
# The owner can read, write and execute, while his group cannot,
# and others can only execute, so that any user can run fakebook.
sudo chmod 701 /usr/local/tomcat -R
sudo chmod 701 /var/lib/mysql -R

# Now we need to install openssh-server
sudo apt-get install openssh-server -y

# Now we will copy a modified version of the file sshd_config
# to the folder it is supposed to be install
sudo cp -f sshd_config /etc/ssh

# Next we restart ssh so that the new things are stored.
sudo /etc/init.d/ssh restart

# Next we will be installing the automatic security upgrades.
# In this section the user will have to say yes to the
# system, when it asks for reconfirmation
sudo apt-get install unattended-upgrades
sudo dpkg-reconfigure unattended-upgrades

# Now we add the banner we want to have appear,
# whenever they are trying to access our IP through ssh
sudo cp -f ssh_banner /etc

# Now we will be configuring the firewall
./firewall_config.h