# First of all we flush the iptables from old rules
sudo iptables -F

#Default Rules
 sudo iptables -P INPUT DROP
 sudo iptables -P FORWARD DROP
 sudo iptables -P OUTPUT DROP

#imcp
 sudo iptables -A INPUT -p icmp --icmp-type -m limit --limit 30/second --limit-burst 200 -j LOG --log-prefix " INCOMING PING REQUEST "
  sudo iptables -A INPUT -p icmp --icmp-type -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A INPUT -p icmp --icmp-type -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM ICMP "

 sudo iptables -A OUTPUT -p icmp --icmp-type -m limit --limit 30/second --limit-burst 200 -j ACCEPT
  sudo iptables -A OUTPUT -p icmp --icmp-type -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK FROM ICMP"

#port 80
 sudo iptables -A INPUT -p tcp -m tcp --dport 80 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
  sudo iptables -A INPUT -p tcp -m tcp --dport 80 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM ICMP "

 sudo iptables -A OUTPUT -p tcp -m tcp --sport 80 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
  sudo iptables -A OUTPUT -p tcp -m tcp --sport 80 -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK FROM ICMP "

#port 443
 sudo iptables -A INPUT -p tcp -m tcp --dport 443 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
  sudo iptables -A INPUT -p tcp -m tcp --dport 443 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM ICMP "

 sudo iptables -A OUTPUT -p tcp -m tcp --sport 443 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
  sudo iptables -A OUTPUT -p tcp -m tcp --sport 443 -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK FROM ICMP "

#port 53
 sudo iptables -A INPUT -p tcp -m tcp --dport 53 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
  sudo iptables -A INPUT -p tcp -m tcp --dport 53 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM ICMP "

 sudo iptables -A OUTPUT -p tcp -m tcp --sport 53 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
  sudo iptables -A OUTPUT -p tcp -m tcp --sport 53 -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK FROM ICMP "

#port 22
 sudo iptables -A INPUT -p tcp -m tcp --dport 22 -m limit --limit 30/second -j LOG --log-prefix " INCOMING SSH "
  sudo iptables -A INPUT -p tcp -m tcp --dport 22 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
   sudo iptables -A INPUT -p tcp -m tcp --dport 22 -m limit --limit 1/minute -j LOG --log-prefix " DOS ATTACK FROM ICMP "

sudo iptables -A INPUT -p tcp -m tcp --sport 22 -m limit --limit 30/second --limit-burst 200 -j LOG --log-prefix " INCOMING SSH "
 sudo iptables -A OUTPUT -p tcp -m tcp --sport 22 -m limit --limit 30/second --limit-burst 200 -j ACCEPT
  sudo iptables -A OUTPUT -p tcp -m tcp --sport 22 -m limit --limit 1/minute -j LOG --log-prefix " DDOS ATTACK FROM ICMP "
