# adding the user called admin
sudo adduser admin

# making sure that the user has been added
cat /etc/passwd

# Changing the owner of tomcat and mysql, to be the admin, and no longer cosec
sudo chown -R admin /usr/local/tomcat

# changing the permissions of the folders mysql and tomcat recursively.
# The owner can read, write and execute, while his group cannot,
# and others can only execute, so that any user can run fakebook.
sudo chmod 701 /usr/local/tomcat -R
  
# changing the privileges of the user admin.
sudo visudo

# Now we need to install openssh-server
sudo apt-get install openssh-server -y

# We go to /etc/ssh/sshd_config and here we change the AllowRootLogin from yes to no
# and we allow user amin to access, by adding the line "AllowUsers admin"
# and possibly we could write "DenyUsers cosec", but we ARE NOT SURE 
# (and users can be exchanged for groups)
sudo vim /etc/ssh/sshd_config

# Then we need to restart ssh, so that the new settings are applied
sudo /etc/init.d/ssh restart

# now to make the automatic security updates, we will install the unattended-upgrades
# package, from aptitude, and the we reconfigure it, using debian package management system
# so that the system updates itself automatically
sudo apt-get install unattended-upgrades
sudo dpkg-reconfigure unattended-upgrades

# Here we are adding a banner that will appear to whoever tries to enter, into the system
# without permission. This is done by changing the BANNER line in the sshd_config file,
# and creating the corresponding file.
sudo touch /etc/SSH_BANNER_PERSONAL.txt
sudo vim /etc/SSH_BANNER_PERSONAL.txt
sudo vim /etc/ssh/sshd_config
