#####################################################
#						    #
# This assignment is made over two scripts. The     #
# first one (this current one) will be concerned    #
# with adding the web admin, and managin his	    #
# rights, as well as installing ssh, and then	    #
# restrictingssh login to the system admin (cosec)  #
# and installing the automatic security upgrades.   #
#						    #
# At the end this script will call the script that  #
# that will configure the firewall, which will in   #
# this case iptables.				    #
#						    #
# ------------------------------------------------- #
# By:						    #
# Shehab Zaineldine (NIA 100287939)		    #
# Alvaro Lopez...				    #
#						    #
#####################################################


# To make sure the scripts are executable
sudo chmod 700 ../scripts -R

# First thing we have to do is to update the passwd
# file. We add the user admin, who will be the web
# admin, responsible for starting up and shutting
# down the server, and managing the mysql database.
sudo useradd -m -s /bin/bash -d /home/Web\ Admin -c "Web Admin" admin
sudo usermod -p $(echo s.z1-2a/l? | openssl passwd -1 -stdin) admin

# Next we need to update the sudoers file.
# Here we have restricted the user web admin from
# having sudo rights outside the tomcat folder.
sudo cp -f ../files/sudoers /etc

# Changing the owner of tomcat and mysql, to be
# the admin, and no longer cosec
sudo chown -R admin /var/lib/mysql
sudo chown -R admin /usr/local/tomcat

# Changing the permissions of the folders mysql
# and tomcat recursively. The owner is the only 
# one that can read, write and execute
sudo chmod 700 /usr/local/tomcat -R
sudo chmod 700 /var/lib/mysql -R

# Now we need to install openssh-server
sudo apt-get install openssh-server -y

# Now we will copy a modified version of the file 
# sshd_config to its folder
sudo cp -f ../files/sshd_config /etc/ssh

# Next we restart ssh so that the new things are stored.
sudo /etc/init.d/ssh restart

# Now we add the banner we want to have appear,
# whenever they are trying to access our IP through ssh
sudo cp -f ../files/ssh_banner /etc/

# Next we will be installing the automatic security upgrades.
# In this section the user will have to say yes to the
# system, when it asks for reconfirmation
sudo apt-get install unattended-upgrades
sudo dpkg-reconfigure unattended-upgrades

# Now we will be configuring the firewall
./firewall_config.h
